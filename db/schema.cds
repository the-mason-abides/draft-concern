using {
  cuid
} from '@sap/cds/common';

entity Customers :  cuid {
  CustomerID: Integer;
  CustomerName: String(30);
}