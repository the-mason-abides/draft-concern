using { Customers as schemaCustomers } from '../db/schema';

service CustomerService{
  entity Customers as projection on schemaCustomers;
  annotate Customers with @odata.draft.enabled;

  action createDraft() returns {ID: UUID};
  
}