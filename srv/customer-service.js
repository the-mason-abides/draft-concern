const cds = require("@sap/cds")

module.exports = cds.service.impl(async (srv) => {
  srv.on("createDraft", async (req) => {
    const { Customers } = srv.entities

    //--this code did not work as part of the original question--
    //const { ID } =  await srv.post(Customers, {})

    //--this code does work to solve the issue--
    const { ID } = await srv.send({query: INSERT.into(Customers).entries({}), event: 'NEW'})
    return { ID }
    
  })
})
